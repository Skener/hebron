<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 20.02.2019
 * Time: 22:10
 */

namespace App\Controllers;

use Core\View;
use App\Comment;

class Comments extends \Core\Controller {

	public function commentform () {
		View::renderTemplate ( 'Comment/index.html' );
	}

	public function newComment () {
		if ( isset( $_POST['comment'] ) && ( ! empty( $_POST['comment'] ) ) ) {
			$comment = new Comment ( $_POST['comment'] );
			$posts   = $comment->getComment ();

			View::renderTemplate ( 'Comment/index.html', [
				'comments' => $posts
			] );
		} else {
			header ( 'Location:/home' );
		}

	}
}