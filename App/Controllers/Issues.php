<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 20.02.2019
 * Time: 22:10
 */

namespace App\Controllers;

use Core\View;
use App\Issue;
use App\Comment;

class Issues extends \Core\Controller {
	public function index () {
		$issues  = new Issue( 'https://redmine.ekreative.com/issues.xml' );
		$id      = $issues->issueList ();
		$dates   = $issues->spentHours ();
		$comment = new Comment ();
		$posts   = $comment->getComment ();
		View::renderTemplate ( 'Issue/index.html', [
			'issues'   => $id,
			'dates'    => $dates,
			'comments' => $posts
		] );
	}
}