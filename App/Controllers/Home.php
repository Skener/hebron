<?php

namespace App\Controllers;

use App\Project;
use \Core\View;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends \Core\Controller {

	/**
	 * Show the index page
	 *
	 * @return void
	 */
	public function home () {
		$projects = new Project( 'https://redmine.ekreative.com/projects.xml' );
		$id       = $projects->projectsList ();

		View::renderTemplate ( 'Home/index.html',
			[
				'projects' => $id
			] );
	}

}