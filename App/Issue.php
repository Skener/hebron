<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 21.02.2019
 * Time: 0:46
 */

namespace App;


use Faker\Provider\DateTime;

class Issue extends Redmine {
	public function __construct ( $url ) {
		parent::__construct ( 'https://redmine.ekreative.com/issues.xml' );
	}

	public function issueList () {

		foreach ( $this->xml->children () as $child ) {
			$childes[]=$child;
		}
		return $childes;

	}


	public function spentHours(){
		$issues = new Issue( 'https://redmine.ekreative.com/issues.xml' );
		$dates     = $issues->issueList ();
		foreach($dates as $date){
			$startDate = $date->start_date;
			$current = date('Y-m-d',time ());
			$datetime1 = new \DateTime($current);
			$datetime2 = new \DateTime($startDate);
			$interval[] = $datetime1->diff($datetime2)->format ('%R%a днів');

		}
		return $interval;
	}

}