<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 20.02.2019
 * Time: 22:09
 */

namespace App;



class Comment {

	public $comments = [];

	public function __construct ( $data=null ) {

		$this->comments = $data;

	}

	public function getComment () {
		return $this->comments;
	}

}