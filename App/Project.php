<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 21.02.2019
 * Time: 23:17
 */

namespace App;


class Project extends Redmine {

	public function __construct ( $url ) {
		parent::__construct ( 'https://redmine.ekreative.com/projects.xml' );
	}

	public function projectsList () {

		foreach ( $this->xml->children () as $child ) {
			$childes[] = $child;
		}

		return $childes;

	}

}