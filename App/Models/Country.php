<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 07.10.2018
 * Time: 18:20
 */

namespace App\Models;

use PDO;

class Country extends \Core\Model {
	public function countryList () {
		$sql  = 'SELECT name  FROM countries';
		$db   = static::getDB ();
		$stmt = $db->query ( $sql );
		return $stmt->fetchAll ();
	}

	public function countryId($id){
		$sql  = 'SELECT *  FROM countries WHERE id = :id';
		$db   = static::getDB ();
		$stmt = $db->prepare($sql);
		$stmt->execute(['id' => $id]);
		return $stmt->fetch();
	}
}