<?php

namespace App\Models;

use PDO;
use \Core\View;

/**
 * User model
 *
 * PHP version 7.0
 */
class User extends \Core\Model {

	/**
	 * Error messages
	 *
	 * @var array
	 */
	public $errors = [];

	/**
	 * Class constructor
	 *
	 * @param array $data Initial property values (optional)
	 *
	 * @return void
	 */
	public function __construct ( $data = [] ) {
		foreach ( $data as $key => $value ) {
			$this->$key = $value;
		};
	}

	public function userList () {
		$sql = 'SELECT u.id, u.username, u.email, u.country_id  FROM users u INNER JOIN countries c ON u.country_id=c.id';
		//$sql = 'SELECT c.id, c.name  FROM countries c JOIN users u  ON  u.country_id = c.id';
		//$sql  = 'SELECT users.id, users.name, users.email FROM users AND  * FROM countries WHERE  users.country_id = countries.id ';
		$db   = static::getDB ();
		$stmt = $db->query ( $sql );

		return $stmt->fetchAll ();
	}


	/**
	 * Save the user model with the current property values
	 *
	 * @return boolean  True if the user was saved, false otherwise
	 */
	public function addUser () {
		$this->validate ();
		if ( empty( $this->errors ) ) {
			$sql  = 'INSERT INTO users (username, email, country_id)
                    VALUES (:username, :email, :country_id)';
			$db   = static::getDB ();
			$stmt = $db->prepare ( $sql );

			$stmt->bindParam ( ':username', $_POST['name'], PDO::PARAM_STR );
			$stmt->bindParam ( ':email', $_POST['email'], PDO::PARAM_STR );
			$stmt->bindParam ( ':country_id', $_POST['country_id'], PDO::PARAM_STR );

			return $stmt->execute ();
		}

		return false;
	}


	public function updateUser ( $id ) {
		$sql  = 'UPDATE users  SET username=:name, email=:email, country_id=:idd WHERE id=:id​';
		$db   = static::getDB ();
		$stmt = $db->prepare ( $sql );

		$stmt->bindValue ( ':name', $_POST['name'], PDO::PARAM_STR );
		$stmt->bindValue ( ':email', $_POST['email'], PDO::PARAM_STR );
		$stmt->bindValue ( ':idd', $_POST['country_id'], PDO::PARAM_STR );
		$stmt->bindValue ( ':id', $id, PDO::PARAM_STR );

		return $stmt->execute ();
	}

	/**
	 * Validate current property values, adding valiation error messages to the errors array property
	 *
	 * @return void
	 */
	public function validate () {
		// Name
		if ( $_POST['name'] == '' ) {
			$this->errors[] = 'Вкажіть ваше імя';
		}

		// email address
		if ( filter_var ( $_POST['email'], FILTER_VALIDATE_EMAIL ) === false ) {
			$this->errors[] = 'Невірний формат email';
		}

	}

	public function delete ( $id ) {
		$sql  = 'DELETE FROM users WHERE id = :id';
		$db   = static::getDB ();
		$stmt = $db->prepare ( $sql );
		$stmt->bindParam ( ':id', $id, PDO::PARAM_INT );

		return $stmt->execute ();

	}

	public function findUser ( $id ) {
		$sql  = 'SELECT *  FROM users WHERE id = :id';
		$db   = static::getDB ();
		$stmt = $db->prepare ( $sql );
		$stmt->execute ( [ 'id' => $id ] );

		return $stmt->fetch ();

	}


}
