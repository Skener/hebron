<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) .'/hebron1/vendor/autoload.php';
$loader = new \Twig_Loader_Filesystem('App/Views');
$twig = new \Twig\Environment($loader, [
	'debug' => true,
	// ...
]);
$twig->addExtension(new \Twig\Extension\DebugExtension());
/**
 * Error and Exception handling
 */
//error_reporting(E_ALL);
//set_error_handler('Core\Error::errorHandler');
//set_exception_handler('Core\Error::exceptionHandler');


/**
 * Sessions
 */
//session_start();


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('/home',['controller'=>'Home', 'action'=>'home'] );
$router->add('/',['controller'=>'Home', 'action'=>'home'] );
$router->add('/issueslist', ['controller' => 'Issues', 'action' => 'index']);
$router->add('/addcomment', ['controller' => 'Comments', 'action' => 'newComment']);
$router->add('/comment', ['controller' => 'Comments', 'action' => 'commentform']);



$router->add('{controller}/{action}');
$router->dispatch($_SERVER['REQUEST_URI']);
