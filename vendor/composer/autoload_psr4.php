<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Http\\Message\\' => array($vendorDir . '/php-http/message/src', $vendorDir . '/php-http/message-factory/src'),
    'Core\\' => array($baseDir . '/Core'),
    'Clue\\StreamFilter\\' => array($vendorDir . '/clue/stream-filter/src'),
    'App\\' => array($baseDir . '/App'),
);
